# SwiftUI Banner and Fullscreen Sample #

This is a sample project demonstrates how to integrate AATKit and implement in-feed banners and fullscreen ads.

Check out our [Wiki](https://bitbucket.org/addapptr/aatkit-ios/wiki/Documentation) for documentation on using AATKit.
