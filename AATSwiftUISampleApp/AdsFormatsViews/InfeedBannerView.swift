//
//  InfeedBannerView.swift
//  AATSwiftUISampleApp
//
//  Created by Mahmoud Amer on 26.10.21.
//

import SwiftUI

struct InfeedBannerView: View {
    var body: some View {
        List {
            ForEach (0...9, id:\.self) { indx in
                if let bannerView = bannerAdView(indx) {
                    bannerView
                        .frame(width: 300, height: 250)
                    
                    Divider()
                } else {
                    AvengersCell(member: Avengers.memberList[indx])
                        .frame(width: 300, height: 200)
                }
                
            }
        }
    }
    
    private func bannerAdView(_ index: Int) -> AATBannerView? {
        switch index {
        case 2:
            return AATBannerView(adUnit: "InfeedBannerPlacement")
        case 5:
            return AATBannerView(adUnit: "InfeedBannerPlacement1")
        case 8:
            return AATBannerView(adUnit: "InfeedBannerPlacement2")
        default:
            return nil
        }
    }
}

//struct InfeedBannerView_Previews: PreviewProvider {
//    static var previews: some View {
//        InfeedBannerView()
//    }
//}
