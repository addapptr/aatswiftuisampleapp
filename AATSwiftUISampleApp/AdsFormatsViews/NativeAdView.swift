//
//  NativeAdView.swift
//  AATSwiftUISampleApp
//
//  Created by Mohamed Matloub  on 02.11.21.
//

import SwiftUI
import AATKit

struct NativeAdView: View {
@StateObject var viewModel = NativeAdViewModel()
    var controller: UIViewController?
    init(controller: UIViewController?) {
        self.controller = controller
    }
    var body: some View {
        List {
            ForEach (0 ..< viewModel.data.count, id:\.self) { index in
                if case let .ad( nativeAd) = self.viewModel.data[index] {
                    AATNativeAdViewWrapper(nativeAd: nativeAd, rootVC: controller)
                        .scaledToFill()
                } else if case let .avenger( avenger) = self.viewModel.data[index] {
                    AvengersCell(member: avenger)
                        .frame(width: 300, height: 200)
                }
            }
        }
    }
}

