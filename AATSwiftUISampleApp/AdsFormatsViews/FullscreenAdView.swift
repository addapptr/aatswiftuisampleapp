//
//  FullscreenAdView.swift
//  AATSwiftUISampleApp
//
//  Created by Mohamed Matloub  on 27.10.21.
//

import SwiftUI

struct FullscreenAdView: View {
    @StateObject var loader = FullscreenAdLoader(placementName: "fullscreen",
                                                 rootVC: UIApplication.shared.getTopViewController())
    
    var body: some View {
        Button {
            switch loader.state {
            case .idle:
                loader.startLoadingAd(from: UIApplication.shared.getTopViewController())
            case .loading:
                break
            case .loaded:
                loader.showAd()
            }
        } label: {
            Text(loader.title)
        }
    }
}
