//
//  AATSwiftUISampleAppApp.swift
//  AATSwiftUISampleApp
//
//  Created by Mahmoud Amer on 26.10.21.
//

import SwiftUI

@main
struct AATSwiftUISampleAppApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
        
    }
}
