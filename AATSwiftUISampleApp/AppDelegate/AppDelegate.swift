//
//  AppDelegate.swift
//  AATSwiftUISampleApp
//
// Created by Mahmoud Amer on 27.01.21.
// Copyright © 2020 AddApptr GmbH. All rights reserved.
//

import UIKit
import AATKit
import AppTrackingTransparency

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        

        // !IMPORTANT! Don't forget to set your own Google AppId in info.plist
        let configuration = AATConfiguration()

        // !IMPORTANT! this line for this demo purpose only and shouldn't be used in live apps
        configuration.testModeAccountId = 1995

        AATSDK.initAATKit(with: configuration)

        AATSDK.setLogLevel(logLevel: .verbose)

        // Start from iOS 14.5, you need to request Tracking Autherization in order to recieve personalized Ads
        ATTrackingManager.requestTrackingAuthorization { status in
            // Tracking authorization completed. Start loading ads here
        }
        return true
    }
}
