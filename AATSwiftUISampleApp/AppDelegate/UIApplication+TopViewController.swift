//
//  UIApplication+TopViewController.swift
//  AATSwiftUISampleApp
//
//  Created by Mohamed Matloub  on 28.10.21.
//

import UIKit

extension UIApplication {
    var currentKeyWindow: UIWindow? {
        UIApplication.shared.connectedScenes
            .filter { $0.activationState == .foregroundActive }
            .map { $0 as? UIWindowScene }
            .compactMap { $0 }
            .first?.windows
            .filter { $0.isKeyWindow }
            .first
    }
    
    /// This is used to get the top visible ViewController,
    /// should follow the Application structure to safely get the top visible ViewController.
    func getTopViewController(base: UIViewController? = UIApplication.shared.currentKeyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
            
        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)
            
        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}
