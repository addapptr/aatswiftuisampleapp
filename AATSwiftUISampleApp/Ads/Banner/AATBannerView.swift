//
//  Banner.swift
//  AATSwiftUISampleApp
//
// Created by Mahmoud Amer on 27.01.21.
//  Copyright © 2020 AddApptr GmbH. All rights reserved.
//

import SwiftUI
import AATKit
import GoogleMobileAds

struct AATBannerView: View {
    var adUnit: String
    
    var body: some View{
        BannerVC(with: adUnit, rootVC: UIApplication.shared.getTopViewController())
    }
}

struct BannerVC: UIViewRepresentable  {

    private var adUnit: String
    weak var rootVC: UIViewController?
    
    static var banners: [String: AdsView] = [:]
    
    init(with adUnit: String, rootVC: UIViewController?) {
        self.adUnit = adUnit
        self.rootVC = rootVC
        guard let rootVC = rootVC else {
            return
        }
        AATSDK.controllerViewDidAppear(controller: rootVC)
    }
    
    func makeUIView(context: Context) -> some UIView {
        defer {
            let key = "\(adUnit)"
            BannerVC.banners[key]?.createAds(adUnit,rootVC: self.rootVC)
        }
        
        let key = "\(adUnit)"
        if let adsVc = BannerVC.banners[key] {
            return adsVc
        }
        
        let adsVC = AdsView()
        
        BannerVC.banners[key] = adsVC
        
        return adsVC
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) { }
}


final class AdsView: UIView {
    var adUnit: String = ""
    var adView: UIView?
    var inFeedBannerPlacement: AATInfeedBannerPlacement?
    
    func addPlaceholder() {
        let placeHolderView = UIImageView(image: UIImage(named: "logo_placeholder_large"))
        placeHolderView.contentMode = .center
        placeHolderView.frame = CGRect(x: 0, y: 0, width: 300, height: 250)
        self.addSubview(placeHolderView)
    }
    
    func createAds(_ adUnit: String, rootVC: UIViewController?) {

        self.adUnit = adUnit
        guard adView == nil else {
            print("There's already an adView: \(String(describing: adView))")
            return
        }
        guard let rootVC = rootVC else {
            print("didn't find any root VC")

            return
        }

        inFeedBannerPlacement = AATSDK.createInfeedBannerPlacement(name: "placement", configuration: AATBannerConfiguration())
        let request = AATBannerRequest(delegate: self)
        request.setRequestBannerSizes(sizes: [AATBannerSize.banner300x250, AATBannerSize.banner320x53])
        inFeedBannerPlacement?.requestAd(request: request, completion: { [weak self]  (view, error) in
            guard let `self` = self,
                let bannerView = view,
                  error == nil else {
                      print("failed with error: \(String(describing: error))")
                      return
                  }
            print("Banner requested successfully")
            self.adView = bannerView
            self.addSubview(self.adView!)
            self.frame = CGRect(origin: .zero, size: self.adView!.frame.size)
            self.frame.size.height = 250
        })
    }
}

extension AdsView: AATBannerRequestDelegate {
    func shouldUseTargeting(for request: AATBannerRequest, network: AATAdNetwork) -> Bool {
        return true
    }
}
