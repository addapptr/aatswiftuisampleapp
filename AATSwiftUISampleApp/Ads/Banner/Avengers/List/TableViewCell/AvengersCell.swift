//
//  AvengersCell.swift
//  AATSwiftUISampleApp
//
//  Created by AddApptr on 26/10/2021.
//

import SwiftUI

struct AvengersCell: View {
    var member : Avengers
    
    var body: some View {
        ZStack{
            NavigationLink(
                destination: AvengerDetail(),
                label: {
                    EmptyView()
                }).frame(width: 0)
                .opacity(0)
            
            HStack {
                Image(member.imageName ?? "avengers-logo")
                    .resizable()
                    .padding(0.0)
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 100)
                    .listRowInsets(EdgeInsets())
                    .border(Color.primary, width: 1)
                    .background(Color.black)
                
                Text(member.name)
                    .font(.system(size:20))
                    .bold()
                    .textCase(.uppercase)
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
                    .lineLimit(nil)
                
            }
        }
    }
}
