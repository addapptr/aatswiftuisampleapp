//
//  Avengers.swift
//  AATSwiftUISampleApp
//
//  Created by AddApptr on 26/10/2021.
//

import Foundation

struct Avengers: Identifiable, Equatable {
    let id = UUID()
    var name: String
    var imageName: String?
    
    static let memberList = [ Avengers(name: "Iron Man", imageName: "Ironman"),
                              Avengers(name: "Captain America", imageName: "CapAmerica"),
                              Avengers(name: "Thor", imageName: "Thor"),
                              Avengers(name: "Hulk", imageName: "Hulk"),
                              Avengers(name: "Black Widow", imageName: "BlackWidow"),
                              Avengers(name: "Hawkeye", imageName: "Hawkeye"),
                              Avengers(name: "Spider-Man", imageName: "Spider-man"),
                              Avengers(name: "Black Panther", imageName: "Black-panther"),
                              Avengers(name: "Ms. Marvel"),
                              Avengers(name: "Kate Bishop")
    ].sorted { avenger1, avenger2 in
        return avenger1.name < avenger2.name
    }
    
    static func ==(lhs: Avengers, rhs: Avengers) -> Bool {
        return lhs.id == rhs.id
    }
}
