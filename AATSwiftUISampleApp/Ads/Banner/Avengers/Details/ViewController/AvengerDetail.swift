//
//  AvengerDetail.swift
//  AATSwiftUISampleApp
//
//  Created by AddApptr on 26/10/2021.
//

import SwiftUI

struct AvengerDetail: View {
    var body: some View {
        ScrollView {
            VStack {
                Image("Avengers-Endgame")
                    .resizable()
                    .padding(.horizontal, 20.0)
                    .aspectRatio(contentMode: .fit)
                
                Spacer()
                    .frame(height:20)
                
                Text("Avengers: Endgame ou Avengers : Phase finale au Québec est un film américain réalisé par Anthony et Joe Russo, sorti en 2019. Il met en scène l'équipe de super-héros des comics Marvel, les Avengers. \n \nIl s'agit du 22e film de l'Univers cinématographique Marvel, débuté en 2008 et du 10e et avant-dernier de la phase III. Ce film est la suite directe de Avengers: Infinity War à la fin duquel « la moitié de tous les êtres vivants de l'univers », dont des personnages de l'Univers Marvel, ont disparu d'un claquement de doigts de Thanos, après qu'il fut entré en possession de toutes les Pierres d'Infinité. \n \nTout comme ses trois prédécesseurs, le film rassemble les acteurs des différentes franchises super-héroïques habituellement séparées, parmi lesquels Iron Man, Black Widow, Hawkeye, Thor, Hulk ou encore Captain America et Ant-Man qui ont survécu à la conclusion du film précédent. Avengers: Endgame marque la fin du cycle des « gemmes de l'infini » démarré avec le film Iron Man en 2008. \n \nLe film effectue le meilleur démarrage de l'histoire du cinéma en rapportant plus de 1,2 milliard de dollars de recettes mondiales lors de son premier week-end d'exploitation. Premier film à dépasser les 2 milliards de dollars onze jours après sa sortie, il devient en douze semaines d'exploitation le plus gros succès du box-office mondial devant Avatar avant que ce dernier ne repasse devant lors d'une ressortie dans les salles de cinéma chinoises en mars 2021.")
                    .padding(.horizontal, 20.0)
                
                Spacer()
                    .frame(height: 30)
                
                Text("Publicité")
                    .foregroundColor(Color.black)
                    .font(.system(size: 18))
                    .bold()
                    .underline()
                    .textCase(.uppercase)
                
                HStack(alignment: .center) {
                    BannerVC(with: "iOSTELProgramBanner", rootVC: UIApplication.shared.getTopViewController())
                        .frame(width: 300, height: 250)
                }
                
                Spacer()
                    .frame(height: 30)
                
                Text("Le Titan Thanos ayant réussi à s'approprier les six Pierres d'Infinité et à les réunir sur le Gantelet doré, a pu réaliser son objectif de pulvériser la moitié de la population de l'Univers d'un claquement de doigts. Les quelques Avengers et Gardiens de la Galaxie ayant survécu, Steve Rogers, Thor, Natasha Romanoff, Tony Stark, Carol Danvers, Clint Barton, Bruce Banner, James Rhodes, Nébula et Rocket espèrent réparer le méfait de Thanos. Ils le retrouvent mais il s'avère que ce dernier a détruit les pierres et Thor le décapite. Cinq ans plus tard, alors que chacun essaie de continuer sa vie et d'oublier les nombreuses pertes dramatiques, Scott Lang, alias Ant-Man, parvient à s'échapper de la Dimension subatomique où il était coincé depuis la disparition du Docteur Hank Pym, de sa femme Janet Van Dyne et de sa fille Hope Van Dyne. Lang propose aux Avengers une solution pour faire revenir à la vie tous les êtres disparus, dont leurs alliés et coéquipiers : récupérer les Pierres d'Infinité dans le passé grâce au Royaume quantique. Pour ce faire, à l'aide des connaissances scientifiques de Bruce Banner et de Tony Stark, ils vont se scinder en plusieurs groupes pour partir chercher les gemmes dans diverses époques passées…")
                    .padding(.horizontal, 20.0)
            }
        }
    }
}
