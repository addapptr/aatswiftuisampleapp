//
//  AATNativeAdView.swift
//  NativeAdSamples
//
//  Created by Mohamed Matloub on 5/4/21.
//

import UIKit
import AATKit
import GoogleMobileAds
import SwiftUI

class AATNativeAdView: UIView {
    //Outlets
    @IBOutlet weak var nativeAdContainerView: GADNativeAdView!
    @IBOutlet weak var adTitleLabel: UILabel!
    @IBOutlet weak var adIconImageView: UIImageView!
    @IBOutlet weak var adMainImageView: UIImageView!
    @IBOutlet weak var adBodyLabel: UILabel!
    @IBOutlet weak var adIdentifierLabel: UILabel!
    @IBOutlet weak var adCTALabel: UILabel!
    
    var nativeAd: AATNativeAdData? {
        didSet {
            updateNativeAd()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        guard let superView = superview else { return }
        superView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        superView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        superView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        superView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
    }
    
    func updateNativeAd() {
        guard let nativeAd = nativeAd else {
            return
        }
        nativeAd.attachToView(nativeAdContainerView, mainImageView: adMainImageView, iconView: adIconImageView, ctaView: adCTALabel)
        bindNativeAd()
    }
    
    func bindNativeAd() {
        guard let nativeAd = nativeAd else {
            return
        }
        // Ad Title
        adTitleLabel.text = nativeAd.title// AATKit.getNativeAdTitle(nativeAd) ?? "-"
        // Ad Icon
        loadImage(for: adIconImageView, imageUrlString: nativeAd.iconUrl)
        // Ad Main Image
        loadImage(for: adMainImageView, imageUrlString: nativeAd.imageUrl)
        // Ad Body
        adBodyLabel.text = nativeAd.description
        // Ad Identifier
        adIdentifierLabel.text = nativeAd.advertiser
        // Ad CTA Title
        adCTALabel.text = nativeAd.callToAction
    }
    
    func loadImage(for imageView: UIImageView, imageUrlString: String?) {
        guard let urlString = imageUrlString,
              let url = URL(string: urlString) else {
                  return
              }
        load(url: url, imageView: imageView)
    }
    
    func load(url: URL, imageView: UIImageView) {
        DispatchQueue.global().async {
            guard let data = try? Data(contentsOf: url),
                  let image = UIImage(data: data) else {
                      return
                  }
            DispatchQueue.main.async {
                imageView.image = image
            }
        }
    }
    
    func setupView() {
        let nibName = "AATNativeAdView"
        guard let contentView = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?.first as? UIView else {
            return
        }
        contentView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(contentView)
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
}
