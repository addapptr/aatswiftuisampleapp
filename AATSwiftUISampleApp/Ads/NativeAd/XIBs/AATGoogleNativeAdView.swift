//
//  AATGoogleNativeAdView.swift
//  NativeAdSamples
//
//  Created by Mohamed Matloub on 6/3/21.
//

import UIKit
import AATKit
import GoogleMobileAds

class AATGoogleNativeAdView: AATNativeAdView {
	@IBOutlet weak var brandingLogoView: UIView!

	var googleMediaView: GADMediaView?

	override func updateNativeAd() {
		super.updateNativeAd()
		setupGoogleAd()
    }
    
	func setupGoogleAd() {
		// Title
		nativeAdContainerView.headlineView = adTitleLabel
		// Body
		nativeAdContainerView.bodyView = adBodyLabel
		//Media View
		updateMediaView()
		// CTA
		nativeAdContainerView.callToActionView = adCTALabel
		// Branding View
		updateBrandingView()
		// Icon
		nativeAdContainerView.iconView = adIconImageView
	}

	override func bindNativeAd() {
		guard let nativeAd = nativeAd else {
			return
		}
		// Ad Title
        adTitleLabel.text = nativeAd.title// AATKit.getNativeAdTitle(nativeAd) ?? "-"
		// Ad Icon
		loadImage(for: adIconImageView, imageUrlString: nativeAd.iconUrl)
        adMainImageView.contentMode = .scaleToFill
		// Ad Body
        adBodyLabel.text = nativeAd.description
		// Ad Identifier
        adIdentifierLabel.text = nativeAd.advertiser
		// Ad CTA Title
        adCTALabel.text = nativeAd.callToAction
	}

	func updateBrandingView() {
//		guard let nativeAd = nativeAd,
//			  let brandingImageView = nativeAd.imageUrl else {
//			return
//		}
//		brandingLogoView.frame = brandingLogoView.bounds
//		brandingLogoView.addSubview(brandingImageView)
	}

	func updateMediaView() {
		googleMediaView?.removeFromSuperview()
		googleMediaView = nil
		adMainImageView.image = nil
		googleMediaView = GADMediaView(frame: adMainImageView.bounds)
		nativeAdContainerView.mediaView = googleMediaView
		guard let googleMediaView = googleMediaView else {
			return
		}
		adMainImageView.addSubview(googleMediaView)
	}
    
    override func setupView() {
        let nibName = "AATGoogleNativeAdView"
        guard let contentView = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?.first as? UIView else {
            return
        }
        contentView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(contentView)
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
}
