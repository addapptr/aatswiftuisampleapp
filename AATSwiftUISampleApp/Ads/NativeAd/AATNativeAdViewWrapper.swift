//
//  AATNativeAdViewWrapper.swift
//  AATSwiftUISampleApp
//
//  Created by Mohamed Matloub  on 03.11.21.
//

import SwiftUI
import AATKit

struct AATNativeAdViewWrapper: UIViewRepresentable {
    var nativeAd: AATNativeAdData
    init(nativeAd: AATNativeAdData, rootVC: UIViewController?) {
        self.nativeAd = nativeAd
        guard let rootVC = rootVC else {
            return
        }
        AATSDK.controllerViewDidAppear(controller: rootVC)
    }
    
    func makeUIView(context: Context) -> UIView {
        if isGoogleAd(nativeAd: nativeAd) {
            let adView: AATGoogleNativeAdView = AATGoogleNativeAdView()
            adView.translatesAutoresizingMaskIntoConstraints = false
            adView.nativeAd = nativeAd
            return adView
        } else {
            let adView = AATNativeAdView()
            adView.translatesAutoresizingMaskIntoConstraints = false
            adView.nativeAd = nativeAd
            return adView
        }
    }

    func isGoogleAd(nativeAd: AATNativeAdData) -> Bool {
        // check if native Ad network is Google
        return nativeAd.network == .DFP || nativeAd.network == .ADMOB
    }

    func updateUIView(_ uiView: UIViewType, context: Context) {
        
    }
}
