//
//  NativeAdLoader.swift
//  AATSwiftUISampleApp
//
//  Created by Mohamed Matloub  on 03.11.21.
//

import UIKit
import AATKit

protocol NativeAdLoaderDelegate {
    func didReceiveAd(nativeAd: AATNativeAdData)
}

class NativeAdLoader {
    var nativeAd: AATNativeAdData?
    var delegate: NativeAdLoaderDelegate?
    
    var nativeAdPlacement: AATNativeAdPlacement?

    init(controller: UIViewController?) {
        if let controller = controller {
            AATSDK.controllerViewDidAppear(controller: controller)
        }
        nativeAdPlacement = AATSDK.createNativeAdPlacement(name: "NativeAdPlacement1", supportsMainImage: true)
        nativeAdPlacement?.delegate = self
        nativeAdPlacement?.reload()
    }
    
    func reload() {
        nativeAdPlacement?.reload()
    }
    
//    func isGoogleAd(nativeAd: AATKitNativeAdProtocol) -> Bool {
//        let network = AATKit.getNativeAdNetwork(nativeAd)
//        return network == AATKitAdNetwork.AATDFP ||
//            network == AATKitAdNetwork.AATAdX ||
//            network == AATKitAdNetwork.AATAdMob
//    }
}

extension NativeAdLoader: AATNativePlacementDelegate {
    func aatPauseForAd(placement: any AATKit.AATPlacement) {
    }
    
    func aatResumeAfterAd(placement: any AATKit.AATPlacement) {
    }
    
    func aatHaveAd(placement: any AATKit.AATPlacement) {
        guard let fetchedNativeAd = nativeAdPlacement?.getNativeAd() else {
            return
        }
        self.nativeAd = fetchedNativeAd
        delegate?.didReceiveAd(nativeAd: fetchedNativeAd)
    }
    
    func aatNoAd(placement: any AATKit.AATPlacement) {
    }
}
