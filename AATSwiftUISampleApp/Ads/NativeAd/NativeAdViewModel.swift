//
//  NativeAdViewModel.swift
//  AATSwiftUISampleApp
//
//  Created by Mohamed Matloub  on 03.11.21.
//

import Combine
import Foundation
import AATKit
import UIKit

enum InfeedType {
    case avenger(avenger: Avengers)
    case ad(nativeAd: AATNativeAdData)
}

class NativeAdViewModel: NativeAdLoaderDelegate, ObservableObject {
    var adsRecursivity = 4
    var nextAdIndex = 3

    
    var loader = NativeAdLoader(controller: UIApplication.shared.getTopViewController())
    @Published var data: [InfeedType]
    
    init() {
        self.data = Avengers.memberList.compactMap({InfeedType.avenger(avenger: $0)})
        loader.delegate = self
    }
    
    func didReceiveAd(nativeAd: AATNativeAdData) {
        if nextAdIndex < data.count - adsRecursivity {
            loader.reload()
        }

        self.data.insert(.ad(nativeAd: nativeAd), at: nextAdIndex)
        nextAdIndex = nextAdIndex + adsRecursivity
    }
}
