//
//  FullscreenAdLoader.swift
//  AATSwiftUISampleApp
//
//  Created by Mohamed Matloub  on 28.10.21.
//

import Combine
import AATKit
import UIKit

class FullscreenAdLoader: NSObject, ObservableObject {
    var placementName: String
    weak var rootVC: UIViewController?
    var state: RequestState = .idle
    @Published var title: String = "Start Loading Ad"
    
    enum RequestState: String {
        case idle
        case loading
        case loaded
    }
    
    var interstitialPlacement: AATFullscreenPlacement?
    
    init(placementName: String, rootVC: UIViewController?) {
        self.rootVC = rootVC
        self.placementName = placementName
        super.init()
        interstitialPlacement = AATSDK.createFullscreenPlacement(name: placementName)
        interstitialPlacement?.delegate = self
        guard let rootVC = rootVC else {
            return
        }
        AATSDK.controllerViewDidAppear(controller: rootVC)
    }
    
    func startLoadingAd(from rootVC: UIViewController?) {
        guard let rootVC = rootVC else {
            return
        }
        AATSDK.controllerViewDidAppear(controller: rootVC)
        interstitialPlacement?.startAutoReload()
        state = .loading
        title = "Loading AD."
    }
    
    func showAd() {
        interstitialPlacement?.show()
        state = .loading
        title = "Loading Ad."
    }
}

extension FullscreenAdLoader: AATFullscreenPlacementDelegate {
    func aatPauseForAd(placement: any AATKit.AATPlacement) {
        print("👉 \(#function)")
    }

    func aatResumeAfterAd(placement: any AATKit.AATPlacement) {
        print("👉 \(#function)")
    }

    func aatHaveAd(placement: any AATKit.AATPlacement) {
        print("👉 \(#function)")
        state = .loaded
        title = "Show Ad."
    }

    func aatNoAd(placement: any AATKit.AATPlacement) {
        print("👉 \(#function)")
    }
}
