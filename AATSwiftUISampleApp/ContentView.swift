//
//  ContentView.swift
//  AATSwiftUISampleApp
//
//  Created by Mahmoud Amer on 26.10.21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            VStack {
                NavigationLink(destination: InfeedBannerView()) {
                    Text("In-Feed Banners")
                }
                
                Divider()
                
                NavigationLink(destination: FullscreenAdView()) {
                    Text("Fullscreen Ad")
                }
                
                Divider()
                
                NavigationLink(destination: NativeAdView(controller: UIApplication.shared.getTopViewController())) {
                    Text("Native Ad")
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
